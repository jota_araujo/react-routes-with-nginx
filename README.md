
Example of configuring nginx to serve a React.js app that uses client-side routing.
Solution based on this article: 
https://medium.com/greedygame-engineering/so-you-want-to-dockerize-your-react-app-64fbbb74c217

### Technologies used

- Docker
- NginX
- React.js
- Typescript
- React Router v4

## Screenshots

Before nginx configured, upon refreshing an internal page, Nginx returns a 404:

   ![screenshot](https://bitbucket.org/jota_araujo/react-routes-with-nginx/raw/acaeed6722e22d75fed166bad3895aca86ee1084/screenshots/nginx0.png)
	
	



After configuration, client side routing works as expected for internal urls :

   ![screenshot](https://bitbucket.org/jota_araujo/react-routes-with-nginx/raw/acaeed6722e22d75fed166bad3895aca86ee1084/screenshots/nginx1.png)



### Instructions

Compile artifacts to /build folder:

```
npm run build
```

Build docker image:
```
docker build . -t react-docker
```

Run docker and navigate to localhost:8000
```
docker run -p 8000:80 react-docker
```

